# Proyecto bases de TypeScript

* Para instalar las dependencias del proyecto (descargará todos los módulos de node necesarios para ejecutarlo):

```
npm install
```


* Una vez instaladas las dependencias, podermos ejecutar el proyecto de con el siguiente comando

```
npm start
```
