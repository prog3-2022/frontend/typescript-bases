let habilidades: string[] = ['Bloqueo', 'Triple', 'Asistencias'];

interface Jugador {
    nombre: string;
    edad: number;
    habilidades: string[];
    equipo?: string;
}


const jugador: Jugador = {
    nombre: 'Lebron',
    edad: 37,
    habilidades: ['Bloqueo', 'Triple', 'Asistencias']
}

jugador.equipo = 'Los Lakers';

console.table(jugador);
