class Persona {

    constructor(
        public id: number,
        public dni: string,
        public nombre: string,
        public email: string
    ) {
    }
}

const persona = new Persona(1, '','','');
